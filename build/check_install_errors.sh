#!/bin/bash
#
# Remove build.failed files
#

# load common functions, compatible with local and installed script
. `dirname $0`/../share/eterbuild/korinf/common


FTPPATH=/var/ftp/pub/Etersoft
PRODLIST="CIFS@Etersoft/{last,testing,stable} HASP/{last,testing,stable} Postgres@Etersoft/{stable,unstable}
RX@Etersoft/{stable,testing,unstable} WINE@Etersoft/{stable,testing,unstable}/WINE Wine-public/last Wine-vanilla/last"

if [ "$1" = "-h" ] ; then
	echo "Run without params for remove from all dirs or with product name as arg"
	exit
fi

check_log()
{
	bzgrep "No package" $@
}

PRODNAME=$1

for p in $PRODLIST ; do
	# skip all exclude PRODNAME if PRODNAME is filled
	if [ -n "$PRODNAME" ] ; then
		echo "$p" | grep -q $PRODNAME || continue
	fi

	VERLIST=$(echo $FTPPATH/$p)
	for v in $VERLIST ; do
		echo "Check in $v"
		#for n in $(get_distro_list $v) ; do
		for n in $v/*/* ; do
			[ -d "$n" ] || continue
			echo "$n" | grep -q "/sources/" && continue
			echo "$n" | grep -q "/x86_64/" && continue
			check_log $n/log/*.log.bz2
		done

		for n in $v/x86_64/*/* ; do
			[ -d "$n" ] || continue
			echo "$n" | grep -q "/sources/" && continue
			check_log $n/log/*.log.bz2
		done
	done
done
