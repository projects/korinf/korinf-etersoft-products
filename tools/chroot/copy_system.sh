#!/bin/sh

# Copyright (c) 2009 Etersoft
# Copyright (c) 2009 Vitaly Lipatov
# Using example:
# ./copy_system.sh SUSE/11.1 11.2
# Copying from /net/os/stable/SUSE/11.1 to /net/os/stable/SUSE/11.2...

SYS=/net/os/stable
FROM=$SYS/$1
TO=$(dirname $FROM)/$2
LISTLINKING="bin
sbin
lib
lib64
lib32
usr
opt"

fatal()
{
	echo $@
	exit 1
}

echo "Copying from $FROM to $TO..."
mkdir -p $FROM
cd $FROM || exit 1
test -d $TO && fatal "Dir $TO already exists, run only without target dir"

LISTDIR=$(ls -1 | grep -v lost+found)
for i in $(echo $LISTDIR) ; do
	test -d $i || continue
	mkdir -p $TO/$i
	if echo $LISTLINKING | grep -q $i ; then
		if [ -L $i ] ; then
			echo "Copy soft link $i..."
			cp -a $i $TO/
		else
			echo "Hard linking $i..."
			cp -al $i $TO/
		fi
	else
		echo "Copying $i..."
		cp -a $i $TO/
	fi
done
