#!/bin/bash

run_script()
{
for os in $DISTR_LIST ; do
    echo $os
    BUILDROOT=stable/$os
    [ -x "$BUILDROOT/bin/sh" ] || continue
    [ -x "$BUILDROOT/root" ] || continue
    cp -af ./remote-script.sh stable/$os/root/remote-script.sh

    echo Mount proc...
    mount -t proc proc $BUILDROOT/proc || exit 1
    chroot stable/$os /root/remote-script.sh
    umount -v $BUILDROOT/proc
done
}

copy_file()
{
for os in $DISTR_LIST ; do
    echo $os
    BUILDROOT=stable/$os
    [ -x "$BUILDROOT/bin/sh" ] || continue
    [ -x "$BUILDROOT/root" ] || continue

    [ -d "$BUILDROOT/dev/" ] || continue

    [ -e "$BUILDROOT/dev/ptmx" ] && continue
    cp -av /dev/ptmx $BUILDROOT/dev/
done
}

export LANG=C

df -h /net/os
export DISTR_LIST=`cd /net/os/stable/i586 ; find -L -maxdepth 2 -mindepth 2 -type d | sed -e "s|^./||" | sort | grep -v Windows`
copy_file
export DISTR_LIST=`cd /net/os/stable/x86_64 ; find -L -maxdepth 2 -mindepth 2 -type d | sed -e "s|^./||" | sort | grep -v Windows`
copy_file

df -h /net/os
