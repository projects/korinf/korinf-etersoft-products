#!/bin/sh

echo "Run script on `distr_vendor`"
DISTRVENDOR=`/usr/bin/distr_vendor -d`
case $DISTRVENDOR in
	"Ubuntu"|"Debian")
		CMD="apt-get clean ; apt-get update"
		#CMD="dpkg -l | grep -i openoffice"
		;;
	"ALTLinux"|"PCLinux")
		CMD="apt-get clean ; apt-get update"
		#CMD="rpm -qa | grep -i openoffice"
		;;
	"LinuxXP"|"Fedora"|"ASPLinux"|"CentOS"|"RHEL"|"Scientific")
		CMD="yum clean all"
		#CMD="rpm -qa | grep -i openoffice"
		;;
	"Mandriva")
		#CMD="rpm -qa | grep -i openoffice"
		;;
	"SUSE"|"NLD")
		#CMD="rpm -qa | grep -i openoffice"
		;;
	*)
		echo "Skipping unknown DISTRVENDOR $DISTRVENDOR"
		;;
esac

$CMD
