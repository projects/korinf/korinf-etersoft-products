#!/bin/sh

GP=/net/os/stable

for d in $(echo $GP/i586/* $GP/x86_64/*) ; do
    echo
    echo "##### Search in $d"
    cd $d || continue
    PREVOS=
    for i in $(echo *) ; do
	if [ -n "$PREVOS" ] ; then
		echo Search in $d/$i and $PREVOS
		for dir in usr bin sbin ; do
        		hardlink -vv $d/$PREVOS/$dir $d/$i/$dir
		done
	fi
	PREVOS=$i
    done
    cd - >/dev/null
done
