#!/bin/sh
export DISTR_LIST=`cd /net/os/stable ; find -L -maxdepth 2 -mindepth 2 -type d 2>/dev/null | sed -e "s|^./||" | sort | grep -v Windows`
echo $DISTR_LIST
