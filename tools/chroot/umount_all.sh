#!/bin/sh
AUTOBUILD=/tmp/autobuild
cd $AUTOBUILD
umount /proc
umount /proc
umount /proc
umount /proc
rm -rf *-home
rmdir *
umount */home/korinfer
rmdir *
umount */*
rmdir *
umount *
rmdir *
mount /proc
lsof | grep $AUTOBUILD
lsof | grep $AUTOBUILD | sed -e "s|.* ||g" | grep -v "umount_all.sh" | grep -v "xargs" | xargs -n1 dirname | grep -v "^.\$" | grep -v "/net/os" | grep -v "/tmp\$" | sort -u >/tmp/tokill_autobuild
cd -
echo
while true ; do
    echo "List to kill uses"
    cat /tmp/tokill_autobuild
    LISTKILL=$(cat /tmp/tokill_autobuild )
    [ -z "$LISTKILL" ] && break
    fuser -vk $LISTKILL
    for i in $LISTKILL ; do
	dirname $i
    done | sort -u | grep "$AUTOBUILD" > /tmp/tokill_autobuild
done
